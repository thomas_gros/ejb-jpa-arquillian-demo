package com.humanbooster.trainings.myjeeapp.jsf.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

import com.humanbooster.trainings.myjeeapp.constraints.Email;
import com.humanbooster.trainings.myjeeapp.ejb.AccountService;
import com.humanbooster.trainings.myjeeapp.entity.Account;

@Named
@ViewScoped
public class AccountRegistration implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@NotNull(message="Please enter a username")
	private String username;
	@NotNull(message="Please enter an email") 
	@Email(message="Please enter a valid email")
	private String email;
	@NotNull(message="Please enter a password")
	private String password;
	
	@Inject
	AccountService accountService;
	
	@PostConstruct // do some stuff after object construction and dependency injection resolved
	public void init() {
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String create() {
		Account account = accountService.createAccount(username, email, password);
		//TODO catch exception => postback or post to error message
		//TODO pas d'exception redirect to homepage
		return "index.xhtml?faces-redirect=true";
		//return "index.xhtml";
	}

}
