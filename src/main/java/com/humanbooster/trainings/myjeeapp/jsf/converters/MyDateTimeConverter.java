package com.humanbooster.trainings.myjeeapp.jsf.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.faces.convert.DateTimeConverter;
import javax.faces.convert.FacesConverter;

/**
 * See http://stackoverflow.com/questions/15239759/jsf-fconvertdatetime-type-date-pattern-mm-dd-yyyy
 */
@FacesConverter("myDateTimeConverter")
public class MyDateTimeConverter extends DateTimeConverter {

	public MyDateTimeConverter() {
		setPattern("MM/dd/yyyy");
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value != null && value.length() != getPattern().length()) {
			throw new ConverterException("Invalid format");
		}

		return super.getAsObject(context, component, value);
	}

}