package com.humanbooster.trainings.myjeeapp.jsf.beans;

import java.util.Date;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class Time {
	public Date getDate() {
		return new Date();
	}
}
