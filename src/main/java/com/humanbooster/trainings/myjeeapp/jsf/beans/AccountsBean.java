package com.humanbooster.trainings.myjeeapp.jsf.beans;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.humanbooster.trainings.myjeeapp.ejb.AccountService;
import com.humanbooster.trainings.myjeeapp.entity.Account;

@RequestScoped
@Named
public class AccountsBean {

	@Inject
	private AccountService accountService;
	
	private int page;
	private boolean hasPrevious;
	private boolean hasNext;
	private int offset;
	private final int maxResultsPerPage = 5;

	public void setPage(int page) {
		this.page = page;
	}
	
	public int getPage() {
		return page;
	}
	
	public void init() {
		//init pagination
		// getAccount count
		long totalAccounts = accountService.countAccounts();
		// check if current page fits
		
		// paginate
		offset = page * maxResultsPerPage;
		hasPrevious = (page > 0);
		hasNext = (offset + maxResultsPerPage < totalAccounts);
	}
	
	public boolean isHasPrevious() {
		return hasPrevious;
	}
	
	public boolean isHasNext() {
		return hasNext;
	}
	
	public List<Account> getAccounts() {
		return accountService.findAllAccounts(offset, maxResultsPerPage);
	}
}
