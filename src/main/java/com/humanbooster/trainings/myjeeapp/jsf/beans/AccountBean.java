package com.humanbooster.trainings.myjeeapp.jsf.beans;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.humanbooster.trainings.myjeeapp.ejb.AccountService;
import com.humanbooster.trainings.myjeeapp.entity.Account;

@Named
@RequestScoped
public class AccountBean {

	private int id;
	
	private Account account;

	@Inject
	AccountService accountService;
	
	public void init() {
		account = accountService.findAccountById(id);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public Account getAccount() {
		return account;
	}
	
	

}
