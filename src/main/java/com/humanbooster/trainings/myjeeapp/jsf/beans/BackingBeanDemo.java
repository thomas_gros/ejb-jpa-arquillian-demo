package com.humanbooster.trainings.myjeeapp.jsf.beans;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.humanbooster.trainings.myjeeapp.ejb.AccountService;
import com.humanbooster.trainings.myjeeapp.entity.Account;

@Named
@RequestScoped
public class BackingBeanDemo {

	@Inject
	AccountService accountService;
	
	Account account;
	
	private String hello = "Hello World";
	
	public String getHello() {
		return hello;
	}
	
	public Account getAccount() {
		if(account == null) {
			account = accountService.findAccountById(20);
		}
		return account;
	}
	
	public List<Account> getAccounts() {
		return accountService.findAllAccounts();
	}
}
