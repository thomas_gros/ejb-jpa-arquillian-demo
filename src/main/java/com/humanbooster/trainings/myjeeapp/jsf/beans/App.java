package com.humanbooster.trainings.myjeeapp.jsf.beans;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class App {

	public String getName() {
		return "My Jee App";
	}
}
