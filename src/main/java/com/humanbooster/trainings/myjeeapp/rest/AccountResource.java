package com.humanbooster.trainings.myjeeapp.rest;

import java.util.List;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.humanbooster.trainings.myjeeapp.ejb.AccountService;
import com.humanbooster.trainings.myjeeapp.entity.Account;

/*   
 * 	GET /accounts => liste des comptes
 *	GET /accounts/{idAccount} => account d'id sp�cifi� en param�tre idAccount
 *	POST /accounts => ajouter un compte
 */
@Path("/accounts")
public class AccountResource {

	@Inject
	AccountService accountService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Account> getAllAccounts() {
		return accountService.findAllAccounts();
	}

	@GET
	@Path("{idAccount}")
	@Produces(MediaType.APPLICATION_JSON)
	public Account getAccountJSON(@PathParam("idAccount") int id) {
		Account account =  accountService.findAccountById(id);
		if (account == null) { throw new WebApplicationException("Can't find account " + id, 404); }
		return account;
	}
	
	@GET
	@Path("{idAccount}")
	@Produces(MediaType.APPLICATION_XML)
	public Response getAccountXML(@PathParam("idAccount") int id) {
		Account account =  accountService.findAccountById(id);
		if (account == null) { throw new WebApplicationException("Can't find account " + id, 404); }
		
		// we'd better use JAXB instead of doing this
		StringBuilder sb = new StringBuilder();
		sb.append("<account>");
		sb.append("<id>").append(account.getId()).append("</id>");
		sb.append("<username>").append(account.getUsername()).append("</username>");
		sb.append("<email>").append(account.getEmail()).append("</email>");
		sb.append("</account>");
		
		return Response.ok(sb.toString()).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public JsonObject createAccount(	@FormParam("username") String username, 
										@FormParam("email") String email, 
										@FormParam("password") String password) {
		Account account = accountService.createAccount(username, email, password);
		
		return Json.createObjectBuilder()
			.add("id", account.getId())
			.add("username", account.getUsername())
			.add("email", account.getEmail())
			.add("url", "/accounts/" + account.getId())
			.build();
	}
	
}
