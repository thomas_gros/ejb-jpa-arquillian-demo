package com.humanbooster.trainings.myjeeapp.rest;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.humanbooster.trainings.myjeeapp.ejb.AccountService;
import com.humanbooster.trainings.myjeeapp.entity.Account;

@Path("/accounts")
public class AccountResource2 {

	@Inject
	AccountService accountService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JsonArray getAccounts() {
		List<Account> accounts = accountService.findAllAccounts();
		
		JsonArrayBuilder b = Json.createArrayBuilder();
		
		for(Account account:accounts) {
			b.add(buildJsonObjectFromAccount(account));
		}
		
		return b.build();
	}

	private JsonObject buildJsonObjectFromAccount(Account account) {
		Objects.requireNonNull(account);

		return Json.createObjectBuilder().add("id", account.getId())
				.add("username", account.getUsername())
				.add("email", account.getEmail()).build();
	}

}
