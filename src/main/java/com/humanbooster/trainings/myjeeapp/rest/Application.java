package com.humanbooster.trainings.myjeeapp.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;


// La classe Application permet (entre autres) de d�clarer les classes correspondant � des ressources REST
@ApplicationPath("/rest")
public class Application extends javax.ws.rs.core.Application {

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> classes = new HashSet<>();
		classes.add(DemoResource.class);
		classes.add(AccountResource.class);
		// sp�cifier les classes qui correspondent � des ressources REST
		return classes;
	}

}