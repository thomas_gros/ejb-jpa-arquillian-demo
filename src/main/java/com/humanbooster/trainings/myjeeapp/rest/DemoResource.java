package com.humanbooster.trainings.myjeeapp.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/demo")
public class DemoResource {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String uneMethodeQuiReagitAuGet() {
		return "<html><body>Coucou</body></html>";
	}
	
}
