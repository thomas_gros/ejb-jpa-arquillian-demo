package com.humanbooster.trainings.myjeeapp.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.humanbooster.trainings.myjeeapp.entity.Account;

/**
 * Session Bean implementation class AccountService
 */
@Stateless
public class AccountService {

	@PersistenceContext(unitName = "jeedemo_pu")
	EntityManager em;

	/**
	 * Retourne le compte correspondant à id.
	 * 
	 * @param id
	 *            identiant du compte
	 * @return le compte correspond à l'id. Retourne null si id ne correspond à
	 *         aucun compte.
	 */
	// debut de transaction transaction.begin
	public Account findAccountById(int id) {
		// account est managé par l'entity manager
		Account account = em.find(Account.class, id);
		return account;
	}// fin de transaction transaction.commit

	public Account createAccount(String userName, String email, String password) {

		// persister un nouvel Account dans la BDD via JPA
		Account account = new Account();
		account.setUsername(userName);
		account.setEmail(email);
		account.setPassword(password);
		em.persist(account);

		// retourner l'objet correspondant au compte nouvellement créé
		return account;
	}

	public List<Account> findAllAccounts() {
		return em.createNamedQuery("Account.findAll", Account.class)
				.getResultList();
	}
	
	public List<Account> findAllAccounts(int offset, int maxResults) {
		return em.createNamedQuery("Account.findAll", Account.class)
				.setFirstResult(offset)
				.setMaxResults(maxResults)
				.getResultList();	
	}
	
	public long countAccounts() {
		return (long) em.createQuery("SELECT COUNT(a) FROM Account a").getSingleResult();
	}

}
