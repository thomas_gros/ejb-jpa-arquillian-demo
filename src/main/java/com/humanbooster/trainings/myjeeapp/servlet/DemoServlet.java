package com.humanbooster.trainings.myjeeapp.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.humanbooster.trainings.myjeeapp.ejb.AccountService;
import com.humanbooster.trainings.myjeeapp.entity.Account;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

/**
 * Servlet implementation class DemoServlet.
 */
@WebServlet("/DemoServlet")
public class DemoServlet extends HttpServlet {
    /**
     * Static id.
     */
    private static final long serialVersionUID = 1L;

    // @Resource(name="java:jboss/datasources/jeedemodatasource")
    // private javax.sql.DataSource dsc;

    /**
     * Account service.
     */
    @Inject
    private AccountService accountService;

    /**
     * Default constructor.
     */
    public DemoServlet() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     *            the request.
     * @param response
     *            the response.
     * @throws ServletException
     *             see HttpServlet#doGet(HttpServletRequest request,
     *             HttpServletResponse response)
     * @throws IOException
     *             see HttpServlet#doGet(HttpServletRequest request,
     *             HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);
        // l'objet account n'est plus managé = il est détaché
        Account account = accountService.findAccountById(1);
        response.getWriter().println(account);

        // account = accountService.createAccount("tutuu", "tutuu@example.com",
        // "1234");
        // response.getWriter().println(account);

        List<Account> accounts = accountService.findAllAccounts();
        for (Account account2 : accounts) {
            response.getWriter().println(account2);
        }

        // jdbcDemo();
        // jdbcDemo2();
        // jpaDemo(request, response);

    }

    /**
     * A sample jpa demo.
     * 
     * @param request
     *            the request.
     * @param response
     *            the response.
     * @throws ServletException
     *             see HttpServlet#doGet(HttpServletRequest request,
     *             HttpServletResponse response)
     * @throws IOException
     *             see HttpServlet#doGet(HttpServletRequest request,
     *             HttpServletResponse response)
     */
    private void jpaDemo(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        List<Account> accounts = accountService.findAllAccounts();
        for (Account account : accounts) {
            response.getWriter().print(
                    String.format("id: %d username: %s email: %s",
                            account.getId(), account.getUsername(),
                            account.getEmail()));
        }
    }

    /**
     * A sample jdbc demo.
     * 
     * @param request
     *            the request.
     * @param response
     *            the response.
     * @throws ServletException
     *             see HttpServlet#doGet(HttpServletRequest request,
     *             HttpServletResponse response)
     * @throws IOException
     *             see HttpServlet#doGet(HttpServletRequest request,
     *             HttpServletResponse response)
     */
    private void jdbcDemo(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().print("Hello world !!! ");
        MysqlDataSource ds = new MysqlDataSource();
        ds.setServerName("localhost");
        ds.setDatabaseName("jeedemo");
        ds.setUser("root");
        ds.setPassword("root");

        try (Connection c = ds.getConnection()) {
            PreparedStatement pstmt = c
                    .prepareStatement("SELECT * FROM account");
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                response.getWriter().print(
                        String.format("id: %d username: %s email: %s",
                                rs.getInt("id"), rs.getString("username"),
                                rs.getString("email")));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Another JDBC demo.
     * 
     * @param request
     *            the request.
     * @param response
     *            the response.
     * @throws ServletException
     *             see HttpServlet#doGet(HttpServletRequest request,
     *             HttpServletResponse response)
     * @throws IOException
     *             see HttpServlet#doGet(HttpServletRequest request,
     *             HttpServletResponse response)
     */
    private void jdbcDemo2(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        // MysqlDataSource ds = new MysqlDataSource();
        // ds.setServerName("localhost");
        // ds.setDatabaseName("jeedemo");
        // ds.setUser("root");
        // ds.setPassword("root");

        // <uniquement pour illustrer l'insertion>
        // try (Connection conn = dsc.getConnection()) {
        //
        // String sql =
        // "INSERT INTO account (username, email, password) VALUES (?, ?, ?)";
        // PreparedStatement pstmt = conn.prepareStatement(sql);
        // pstmt.setString(1, "myuser");
        // pstmt.setString(2, "myuser@mymail.com");
        // pstmt.setString(3, "myuserpassword");
        //
        // pstmt.executeUpdate();
        //
        // } catch (SQLException e) {
        // response.setStatus(500);
        // response.getWriter().print(e.getMessage());
        // e.printStackTrace();
        // return;
        // // et aussi en profiter pour envoyer un
        // // message aux développeurs
        // }
        //
        // // </uniquement pour illustrer l'insertion>
        //
        //
        // // Parce que Connection implements Closeable
        // // on peut utiliser la syntaxe Java SE 7 try with ressources
        // try (Connection conn = dsc.getConnection()) {
        // String idFromRequest = request.getParameter("id");
        // // traiter la variable venant de l'ext�rieur avant de l'injecter dans
        // la
        // // requete
        // PreparedStatement pstmt =
        // conn.prepareStatement("SELECT * FROM account WHERE id = ?");
        //
        // try {
        // pstmt.setInt(1, Integer.valueOf(idFromRequest));
        // } catch (NumberFormatException e) {
        // response.setStatus(404);
        // response.getWriter().print(e.getMessage());
        // return;
        // }
        //
        // ResultSet rs = pstmt.executeQuery();
        // while(rs.next()) {
        // Integer id = rs.getInt("id");
        // String username = rs.getString("username");
        //
        // response.getWriter().printf("id: %d username: %s", id, username);
        // }
        //
        // } catch (SQLException e) {
        // response.setStatus(500);
        // response.getWriter().print(e.getMessage());
        // e.printStackTrace();
        // // et aussi en profiter pour envoyer un
        // // message aux développeurs
        // }
    }

}
