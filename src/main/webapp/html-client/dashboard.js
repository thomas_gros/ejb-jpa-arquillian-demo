/* This is the client-side script
// 
//// Initialize the Ajax request
//var xhr = new XMLHttpRequest();
//xhr.open('get', 'http://localhost:8080/my-jee-app-0.0.1-SNAPSHOT/rest/accounts');
// 
//// Track the state changes of the request
//xhr.onreadystatechange = function(){
//    // Ready state 4 means the request is done
//    if(xhr.readyState === 4){
//        // 200 is a successful return
//        if(xhr.status === 200){
//        	// alert(xhr.responseText); // 'This is the returned text.'
//        	// document.getElementById('accounts').innerHTML = xhr.responseText;
//        	
//        	var accounts = JSON.parse(xhr.responseText);
//        	//console.log(accounts);
//        	var html = "<ul>";
//        	for(var i = 0; i < accounts.length; i++) {
//        		var account = accounts[i];
//        		html += "<li>" + account.username + "</li>";
//        	}
//        	html += "</ul>";
//
//        	document.getElementById('accounts').innerHTML = html;
//        	
//        }else{
//            alert('Error: '+xhr.status); // An error occurred during the request
//        }
//    }
//}
// 
//// Send the request to send-ajax-data.php
//xhr.send(null);
*/

$.getJSON("http://localhost:8080/my-jee-app-0.0.1-SNAPSHOT/rest/accounts", function(accounts) {
	var html = "<ul>";
	for(var i = 0; i < accounts.length; i++) {
		var account = accounts[i];
		html += "<li>" + account.username + "</li>";
	}
	html += "</ul>";

	$("#accounts").html(html);
});



























//var updateTable = function() {
//	$("#accounts").append("updating...");
//	$.getJSON("http://localhost:8080/my-jee-app-0.0.1-SNAPSHOT/rest/accounts", function(data) {
//		var items = [];
//		  $.each( data, function( key, val ) {
//		    items.push( "<tr>" +
//				    		"<td>" + val.id + "</td>" +
//				    		"<td>" + val.username + "</td>" +
//				    		"<td>" + val.email + "</td>" +
//		    			"</tr>");
//		  });
//		 
//		  
//		  $("#accounts").html(
//				  $("<table/>", {
//					  "class": "table",
//					  html: items.join("")
//				  })
//		  );	  
//	});
//}
//
//setInterval(updateTable,1000);