package com.humanbooster.trainings.myjeeapp.ejb;

import java.util.logging.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.humanbooster.trainings.myjeeapp.entity.Account;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class AccountServiceTest {
	static Logger logger = Logger
			.getLogger(AccountServiceTest.class.toString());

	@Inject
	AccountService accountService;

	@Inject
	UserTransaction utx;

	@PersistenceContext(name = "jeedemo_pu")
	EntityManager em;

	@Deployment
	public static JavaArchive createDeployment() {
		JavaArchive jar = ShrinkWrap
				.create(JavaArchive.class)
				.addClass(AccountService.class)
				.addClass(Account.class)
				// .addAsManifestResource("META-INF/persistence.xml",
				// "persistence.xml")
				.addAsResource("test-persistence.xml",
						"META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
		System.out.println(jar.toString(true));

		return jar;
	}

	/**
	 * Inspired from http://arquillian.org/guides/testing_java_persistence/
	 */
	@Before
	public void beforeEveryMethods() {
		try {
			utx.begin();
			em.joinTransaction();
			em.createNativeQuery("delete from account").executeUpdate(); // clear database
			utx.commit();
		} catch (Exception ex) {
			// Arquillian seems to "swallow" the exceptions inside @Before
			// Furthermore, if an exception is raised every test will be successful... 
			// without any other information :-(
			
			// at least here we catch every exception and we will be able to see 
			// it inside the server's log file...
			ex.printStackTrace();
		}
	}

	@After
	public void afterEveryMethods() {
		// fill test db
	}

	@Test
	public void testSaysHello() throws Exception {
		String userName = "t1";
		String email = "t1@example.com";
		String password = "t1pass";
		Account account = accountService.createAccount(userName, email,
				password);

		assertNotNull("id must not be null " + account.getId());
		assertEquals("username should be " + userName, account.getUsername(),
				userName);
		assertEquals("email should be " + email, account.getEmail(), email);
		assertEquals("password should be " + password, account.getPassword(),
				password);

		utx.begin();
		em.joinTransaction();
		Account accountFromDB = em.find(Account.class, account.getId());
		utx.commit();

		assertNotNull("account with id " + account.getId()
				+ " must exist inside the database", accountFromDB);
		assertEquals(
				"Account created must be equals to the one found in the database",
				account, accountFromDB);
	}

}
